module.exports = {
  extends: 'airbnb',
  rules: {
    'react/jsx-uses-vars': 1,
    'react/jsx-filename-extension': 0,
    indent: ['error', 2],
    quotes: ['error', 'single'],
    semi: ['error', 'never'],
  },
}
