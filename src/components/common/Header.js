import React, { Component } from 'react';
import Avatar from 'material-ui/Avatar';
import AccountCircleIcon from 'material-ui/svg-icons/action/account-circle';
import AppBar from 'material-ui/AppBar';

export default class Header extends Component {
  render() {
    const appBarStyles = {
      backgroundColor: '#444'
    };

    return (
      <div className="top-bar">
        <AppBar
          title="Site Title"
          showMenuIconButton={false}
          style={appBarStyles}
          iconElementRight={<Avatar icon={<AccountCircleIcon />} />}
        />
      </div>
    );
  }
}
