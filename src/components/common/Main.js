import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import Bucketlists from '../Bucketlists';
import BucketlistItems from '../BucketlistItems';
import Login from '../auth/Login';
import Register from '../auth/Register';
import { connect } from 'react-redux';

class Main extends Component {

  render() {
    return (
      <Switch>
        <Route exact path='/' render={() => (
          this.props.isLoggedIn ? (<Redirect to='/bucketlists' />) : (<Login />)
        )} />
        <Route exact path='/login'  component={Login} />
        <Route exact path='/register'  component={Register} />
        <Route exact path='/bucketlists'  component={Bucketlists} />
        <Route exact path='/bucketlists/:number/items'  component={BucketlistItems} />
      </Switch>
    )
  }
}


export default connect()(Main);