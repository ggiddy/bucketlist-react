import React, { Component } from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';

export default class Login extends Component {
  render() {
    return (
      <div className="container">
        <div className="login-form">
          <div style={{ textAlign: 'center', marginTop: '8em' }}>
            <TextField
              fullWidth={true}
              type="text" 
              floatingLabelText="First Name*" /><br/>
            <TextField
              fullWidth={true}
              type="text" 
              floatingLabelText="Last Name*" /><br/>
            <TextField
              fullWidth={true}
              type="email"
              floatingLabelText="Email*" /><br/>
            <TextField
              fullWidth={true}
              type="password"
              floatingLabelText="Password*"
            />
            <TextField
              fullWidth={true}
              type="password"
              floatingLabelText="Confirm Password*"
            />
            <br />
            <br />
            <RaisedButton label="Register" primary fullWidth={true} />
          </div>
        </div>
      </div>
    );
  }
}
