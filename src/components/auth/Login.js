import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'
import * as loginActions from '../../actions/loginActions'

class Login extends Component {
  constructor(props) {
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleEmailChange = this.handleEmailChange.bind(this)
    this.handlePasswordChange = this.handlePasswordChange.bind(this)
    this.state = {
      email: '',
      password: '',
    }
  }

  handleSubmit(event) {
    event.preventDefault()
    this.props.actions.loginRequest(this.state)
  }

  handleEmailChange(event) {
    const email = event.target.value
    this.setState({ email })
  }

  handlePasswordChange(event) {
    const password = event.target.value
    this.setState({ password })
  }

  render() {
    return (
      <div className="container">
        <div className="login-form">
          <div style={{ textAlign: 'center', marginTop: '8em' }}>
            <h1>Sign In</h1>
            <form onSubmit={this.handleSubmit}>
              <TextField
                fullWidth
                type="email"
                floatingLabelText="Email*"
                onChange={this.handleEmailChange}
                value={this.state.email}
              />
              <br />
              <TextField
                fullWidth
                type="password"
                floatingLabelText="Password*"
                onChange={this.handlePasswordChange}
                value={this.state.password}
              />
              <br />
              <br />
              <RaisedButton
                type="submit"
                label="Login"
                primary
                fullWidth
              />
            </form>
          </div>
        </div>
      </div>
    )
  }
}

export default connect()(Login)
