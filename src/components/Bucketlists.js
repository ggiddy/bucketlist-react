import React, { Component } from 'react';
import TextField from 'material-ui/TextField';
import { List, ListItem } from 'material-ui/List';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

export default class Bucketlists extends Component {
  constructor(props) {
    super(props);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  state = {
    open: false
  };

  handleEdit() {
    return this.setState({ open: true });
  }

  handleClose() {
    return this.setState({ open: false });
  }

  handleSave() {
    return this.setState({ open: false });
  }

  render() {
    const iconButtonElement = (
      <IconButton touch={true} tooltip="Options" tooltipPosition="top-left">
        <MoreVertIcon />
      </IconButton>
    );

    const rightIconMenu = (
      <IconMenu iconButtonElement={iconButtonElement}>
        <MenuItem onClick={this.handleEdit}>Edit</MenuItem>
        <MenuItem>Delete</MenuItem>
      </IconMenu>
    );

    const dialogActions = [
      <FlatButton label="Cancel" primary onClick={this.handleClose} />,
      <FlatButton label="Save" primary onClick={this.handleSave} />
    ];

    return (
      <div>
        <Dialog
          title="Edit item"
          actions={dialogActions}
          modal={false}
          open={this.state.open}
          children={
            <TextField name="edit" fullWidth={true} value="Travelling" />
          }
          onRequestClose={this.handleClose}
        />
        <div className="container">
          <div className="main">
            <TextField
              fullWidth={true}
              hintText="Enter bucketlist name and hit enter to create..."
            />

            <div className="bucketlists">
              <List>
                <ListItem
                  primaryText="Travelling"
                  rightIcon={rightIconMenu}
                  secondaryText={<p />}
                  secondaryTextLines={2}
                />
                <ListItem
                  primaryText="Travelling"
                  rightIcon={rightIconMenu}
                  secondaryText={<p />}
                  secondaryTextLines={2}
                />
                <ListItem
                  primaryText="Travelling"
                  rightIcon={rightIconMenu}
                  secondaryText={<p />}
                  secondaryTextLines={2}
                />
                <ListItem
                  primaryText="Travelling"
                  rightIcon={rightIconMenu}
                  secondaryText={<p />}
                  secondaryTextLines={2}
                />
                <ListItem
                  primaryText="Travelling"
                  rightIcon={rightIconMenu}
                  secondaryText={<p />}
                  secondaryTextLines={2}
                />
              </List>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
