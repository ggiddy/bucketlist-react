import React, { Component } from 'react';
import TextField from 'material-ui/TextField';
import Toggle from 'material-ui/Toggle';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import DateRange from 'material-ui/svg-icons/action/date-range';

import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn
} from 'material-ui/Table';

export default class Bucketlists extends Component {
  constructor(props) {
    super(props);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  state = {
    open: false
  };

  handleEdit() {
    return this.setState({ open: true });
  }

  handleClose() {
    return this.setState({ open: false });
  }

  handleSave() {
    return this.setState({ open: false });
  }

  render() {
    const iconButtonElement = (
      <IconButton touch={true} tooltip="Options" tooltipPosition="top-left">
        <MoreVertIcon />
      </IconButton>
    );

    const rightIconMenu = (
      <IconMenu iconButtonElement={iconButtonElement}>
        <MenuItem onClick={this.handleEdit}>Edit</MenuItem>
        <MenuItem>Delete</MenuItem>
      </IconMenu>
    );

    const dialogActions = [
      <FlatButton label="Cancel" primary onClick={this.handleClose} />,
      <FlatButton label="Save" primary onClick={this.handleSave} />
    ];

    return (
      <div>
        <Dialog
          title="Edit item"
          actions={dialogActions}
          modal={false}
          open={this.state.open}
          children={
            <TextField name="edit" fullWidth={true} />
          }
          onRequestClose={this.handleClose}
        />
        <div className="container">
          <div className="main">
            <TextField fullWidth={true} hintText="Item name" />
            <Toggle label="Show complete?" labelPosition="right" />
            <div className="bucketlists">
              <Table
                fixedHeader={true}
                fixedFooter={true}
                multiSelectable={true}
              >
                <TableHeader adjustForCheckbox={true} enableSelectAll={false}>
                  <TableRow>
                    <TableHeaderColumn>Item Name</TableHeaderColumn>
                    <TableHeaderColumn tooltip="Date Created">
                      <DateRange />
                    </TableHeaderColumn>
                    <TableHeaderColumn>Options</TableHeaderColumn>
                  </TableRow>
                </TableHeader>
                <TableBody
                  displayRowCheckbox={true}
                  deselectOnClickaway={false}
                  showRowHover={true}
                >
                  <TableRow>
                    <TableRowColumn>Mombasa</TableRowColumn>
                    <TableRowColumn>2 days ago</TableRowColumn>
                    <TableRowColumn>{rightIconMenu}</TableRowColumn>
                  </TableRow>
                  <TableRow>
                    <TableRowColumn>Mombasa</TableRowColumn>
                    <TableRowColumn>2 days ago</TableRowColumn>
                    <TableRowColumn>{rightIconMenu}</TableRowColumn>
                  </TableRow>
                  <TableRow>
                    <TableRowColumn>Mombasa</TableRowColumn>
                    <TableRowColumn>2 days ago</TableRowColumn>
                    <TableRowColumn>{rightIconMenu}</TableRowColumn>
                  </TableRow>
                  <TableRow>
                    <TableRowColumn>Mombasa</TableRowColumn>
                    <TableRowColumn>2 days ago</TableRowColumn>
                    <TableRowColumn>{rightIconMenu}</TableRowColumn>
                  </TableRow>
                  <TableRow>
                    <TableRowColumn>Mombasa</TableRowColumn>
                    <TableRowColumn>2 days ago</TableRowColumn>
                    <TableRowColumn>{rightIconMenu}</TableRowColumn>
                  </TableRow>
                </TableBody>
              </Table>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
