import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import { BrowserRouter } from 'react-router-dom';

import App from './containers/App';
import registerServiceWorker from './registerServiceWorker';
import { store } from './store';

import './index.css';

ReactDOM.render(
  <MuiThemeProvider muiTheme = {getMuiTheme(lightBaseTheme)}>
    <BrowserRouter>
      <Provider store = {store} >
        <App />
      </Provider>
    </BrowserRouter>
  </MuiThemeProvider>,
  document.getElementById('root')
);
registerServiceWorker();
