import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import { createLogger } from 'redux-logger';
import reducer from '../reducers';
import { loginSaga } from '../sagas';
import freeze from 'redux-freeze'

const sagaMiddleware   = createSagaMiddleware();
const loggerMiddleware = createLogger();

const middleware       = {
  loggerMiddleware,
  freeze
};

const composeEnhancers = composeWithDevTools(...middleware);


export const store     = createStore(
  reducer,
  composeEnhancers,
  applyMiddleware(sagaMiddleware)
);

sagaMiddleware.run(loginSaga);
