import { LOGIN_REQUEST } from '../constants/actionTypes';
export function loginRequest({ email, password }) {
  return {
    type: LOGIN_REQUEST,
    email,
    password
  }
}