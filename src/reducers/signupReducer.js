import { initialState } from './initialState';
import * as types from '../constants/actionTypes';

export function auth(state = initialState, action) {
  switch (action.type) {
    case types.LOGIN_REQUEST:
      return { ...state };

    case types.LOGIN_SUCCEESSFUL:
      return  { ...state, user: { jwtToken: action.token, isLoggedIn: true } }

    case types.LOGIN_FAILED:
      return { ...state, error: { message: action.message} }

    default:
      return state;
  }
}