export const initialState = {
  user: {
    firstName: '',
    lastName: '',
    email: '',
    jwtToken: '',
    isLoggedIn: false
  },
  bucketlists: [],
  error: {}
};