import API_ENDPOINT from '../constants/apiEndpoints';
import axios from 'axios';
/**
 * login
 * register
 * create bucketlist
 * read bucketlists
 * update bucketlist
 * delete bucketlist
 * 
 * create bucketlist item
 * read bucketlist items
 * update bucketlist item
 * delete bucketlist item
 * 
 */

export function login(loginCredentials) {
  return axios.post(`${API_ENDPOINT}/auth/login`, loginCredentials)
    .then(({ data }) => {
      return data.access_token;
    });
}