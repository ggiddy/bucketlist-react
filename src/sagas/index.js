import { LOGIN_FAILED, LOGIN_REQUEST, LOGIN_SUCCEESSFUL } from '../constants/actionTypes';

import { call, put, takeLatest } from 'redux-saga/effects';
import * as Api from './Api';


function* loginUser(action) {
  try {
    const jwtToken = yield call(Api.login, action.loginCredentials);
    yield put({ type: LOGIN_SUCCEESSFUL, token: jwtToken });
  } catch (error) {
    yield put({ type: LOGIN_FAILED, message: error });
  }
}

export function* loginSaga() {
  yield takeLatest(LOGIN_REQUEST, loginUser);
}
